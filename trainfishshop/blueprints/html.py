"""HTML Routes"""

#pylint: disable=missing-docstring

import stripe

from flask import Blueprint, render_template, redirect, request, flash, abort
from flask_login import current_user, login_user, logout_user
from requests import post, get

from trainfishshop.common import loginrequired, adminrequired, securelink
from trainfishshop.models import User, Purchase, Product, File
from trainfishshop.database import DB
from trainfishshop.config import _cfg

HTML = Blueprint("HTML", __name__, template_folder="../../templates")

@HTML.route("/")
def index():
    return render_template("index.html")

@HTML.route("/login")
def login():
    return redirect("https://github.com/login/oauth/authorize?client_id={}"
                    .format(_cfg("GithubClientId")))

@HTML.route("/product/<pid>")
def product(pid: str):
    prod = Product.query.filter(Product.id == pid).first()
    if not prod:
        abort(404)
    return render_template("product.html", p=prod,
                           owned=[i.id for i in current_user.purchases] if current_user else [])

@HTML.route("/logout")
def logout():
    logout_user()
    return redirect("/")

@HTML.route("/_oauth/callback")
def oauth_callback():
    res = post("https://github.com/login/oauth/access_token?client_id={}&client_secret={}&code={}"
               .format(_cfg("GithubClientId"), _cfg("GithubClientSecret"), request.args["code"]),
               headers={"Accept": "application/json"})
    if "error" in res:
        flash("Failed to log in", "danger")
        return redirect("/")
    obj = get("https://api.github.com/user?access_token=" + res.json()["access_token"]).json()

    user = User.query.filter(User.id == obj["id"]).first()
    if not user:
        #pylint: disable=no-member
        user = User(obj)
        DB.add(user)
        DB.commit()

    login_user(user)

    return redirect("/")

@HTML.route("/account")
@loginrequired
def account():
    return render_template("account.html")

@HTML.route("/admin")
@adminrequired
def admin():
    return render_template("admin.html")

@HTML.route("/checkout", methods=["POST"])
@loginrequired
def checkout():
    prod = Product.query.filter(Product.id == request.form["productId"]).first()
    if not prod:
        abort(404)
    try:
        charge = stripe.Charge.create(
            amount=product.price,
            currency="usd",
            description="Purchase of " + product.name,
            source=request.form["stripeToken"]
        )
    except stripe.error.CardError as err:
        flash(err.json_body["error"]["message"], "danger")
        return redirect("/")
    except stripe.error.InvalidRequestError as err:
        flash(err.json_body["error"]["message"], "danger")
        return redirect("/")
    flash("Thank you for your purchase!", "success")
    purch = Purchase(current_user.id, request.form["productId"], charge)
    #pylint: disable=no-member
    DB.add(purch)
    DB.commit()
    return redirect("/account")

@HTML.route("/download/<fid>")
@loginrequired
def download(fid: str):
    fil = File.query.filter(File.id == fid).first()
    if not fil:
        abort(404)
    owned_products = [i.product_id for i in current_user.purchases]
    if fil.project_id not in owned_products:
        flash(f"You do not own {fid}!", "danger")
        return redirect("/account")
    return redirect(_cfg("DownloadUrl") + securelink(fil.url, request.remote_addr))
