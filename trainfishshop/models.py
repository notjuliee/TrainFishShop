"""Models for database"""

#pylint: disable=too-few-public-methods,missing-docstring

from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, Boolean, Text
from sqlalchemy.orm import relationship

from trainfishshop.database import Base

class User(Base):
    """User class"""
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    username = Column(String(256))
    avatar_url = Column(String(256))
    created = Column(DateTime)
    admin = Column(Boolean)

    purchases = relationship("Purchase", primaryjoin="and_(User.id==Purchase.user_id)",
                             backref="users")

    def __init__(self, obj: dict):
        self.id = obj["id"] #pylint: disable=invalid-name
        self.username = obj["login"]
        self.avatar_url = obj["avatar_url"]
        self.created = datetime.utcnow()
        self.admin = False

    def __repr__(self):
        return f"<User {self.username}>"

    @staticmethod
    def is_authenticated():
        """For flask_auth"""
        return True
    @staticmethod
    def is_active():
        """For flask_auth"""
        return True
    @staticmethod
    def is_anonymous():
        """For flask_auth"""
        return False
    def get_id(self):
        """For flask_auth"""
        return self.id

class Purchase(Base):
    """Purchase Class"""
    __tablename__ = "purchases"

    id = Column(String, primary_key=True)
    created = Column(DateTime)
    amount = Column(Integer)
    product_id = Column(Integer, ForeignKey("products.id"))
    user_id = Column(Integer, ForeignKey("users.id"))

    def __init__(self, uid: str, pid: str, obj: dict):
        self.id = obj["id"] #pylint: disable=invalid-name
        self.created = datetime.fromtimestamp(obj["created"])
        self.amount = obj["amount"]
        self.product_id = pid
        self.user_id = uid

    def __repr__(self):
        return f"<Purchase {self.id} by {self.user_id}>"

class Product(Base):
    __tablename__ = "products"

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(256))
    price = Column(Integer)
    snippet = Column(Text)
    description = Column(Text)

    purchases = relationship("Purchase", primaryjoin="and_(Product.id==Purchase.product_id)",
                             backref="products")
    files = relationship("File", primaryjoin="and_(Product.id==File.product_id)",
                         backref="products")

    def __repr__(self):
        return f"<Product {self.name} ({self.id})>"

class File(Base):
    __tablename__ = "files"

    id = Column(Integer, primary_key=True, autoincrement=True)
    product_id = Column(Integer, ForeignKey("products.id"))
    name = Column(String(256))
    description = Column(Text)
    url = Column(String(256))
    version = Column(String(256))

    def __repr__(self):
        return f"<File {self.name} ({self.id}) for {self.product_id}>"
