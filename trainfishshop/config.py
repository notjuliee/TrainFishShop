"""config.ini wrapper"""

from configparser import ConfigParser

CONFIG = ConfigParser()
CONFIG.read("config.ini")

## Get a value from config.ini
# @param val Key to read from
# @param t Type of value
# @return Value of val in type t
def _cfg(val: str, val_type=str):
    if val_type == bool:
        return CONFIG.getboolean("cfg", val)
    return val_type(CONFIG.get("cfg", val))
